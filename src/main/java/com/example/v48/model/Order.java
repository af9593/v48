package com.example.v48.model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;
import javax.persistence.JoinColumn;
import java.util.List;


@Entity
@Table(name="ORDER_TABLE")
@Getter
@Setter
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long order_id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer ;

    @ManyToMany
    @JoinColumn(name = "product_id")
    private List<Product> productList;


}
