package com.example.v48.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ProductExtended {
    private long product_id;

    private String label ;

    private String description ;

    private int price;

    private List<Recommendation> recommendations;
}
