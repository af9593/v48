package com.example.v48.model;

import lombok.*;

@Getter
@Setter
public class Recommendation {
    private long id ;

    private long productId ;

    private String productName;

    private String customerName;

    private String recommendation;
}
