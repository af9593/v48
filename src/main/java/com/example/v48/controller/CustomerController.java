package com.example.v48.controller;

import com.example.v48.model.Customer;
import com.example.v48.model.Order;
import com.example.v48.repository.CustomerRepository;
import com.example.v48.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    OrderRepository orderRepository;

    @GetMapping("/all")
    public List<Customer> getCustomers(){
        return customerRepository.findAll();
    }



    @GetMapping("{id}")
    public Customer getCustomer(@PathVariable long id ){  return customerRepository.getById(id); }

    @GetMapping("/orders/{id}")
    public List<Order> getCustomerOrders(@PathVariable long id){

        return  orderRepository.findAll().stream().filter(o -> o.getCustomer().getCustomerId() == id).findAny().stream().toList();
    }

    @PostMapping
    public void addCustomer(@RequestBody Customer customer) {
        Optional<Customer> existingCustomer =  customerRepository.findById(customer.getCustomerId());
        if(existingCustomer.isEmpty())  customerRepository.save(customer);
    }

    @DeleteMapping("/{id}")
    public void removeCustomer(@PathVariable long id ) {
        try {
            Customer existingCustomer  =  customerRepository.findAll().stream().filter(c -> c.getCustomerId() == id).findFirst().get();
            customerRepository.deleteById(id);
        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public void updateCustomer(@RequestBody Customer customer , @PathVariable long id ){
        try {
            customerRepository.findAll().stream().filter(c -> c.getCustomerId() == id ).findFirst().get();
            if(customer.getCustomerId() == id) customerRepository.save(customer);
        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
        }
    }
}
