package com.example.v48.controller;

import com.example.v48.model.Order;
import com.example.v48.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("api/order")
public class OrderController {


    @Autowired
    OrderRepository orderRepository;

    @GetMapping("/all")
    public List<Order> getOrders(){
        return orderRepository.findAll();
    }

    @GetMapping("{id}")
    public Order getOrder(@PathVariable long id ){  return orderRepository.getById(id); }

    @PostMapping
    public void addOrder(@RequestBody Order order) {
        Optional<Order> existingOrder =  orderRepository.findById(order.getOrder_id());
        if(existingOrder.isEmpty())  orderRepository.save(order);
    }

    @DeleteMapping("/{id}")
    public void removeOrder(@PathVariable long id ) {
        try {
            Order existingOrder  =  orderRepository.findAll().stream().filter(o -> o.getOrder_id() == id).findFirst().get();
            orderRepository.deleteById(id);
        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void updateOrder(@RequestBody Order order , @PathVariable long id ){
        try {
            //Checks if element with the given id exists
            orderRepository
                    .findAll().stream()
                    .filter(c -> c.getOrder_id() == id)
                    .findFirst().get();


            if(order.getOrder_id() == id)  orderRepository.save(order);

        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
        }
    }

}
