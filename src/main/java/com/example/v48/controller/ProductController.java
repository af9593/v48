package com.example.v48.controller;

import com.example.v48.model.Product;
import com.example.v48.model.Recommendation;
import com.example.v48.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.net.URISyntaxException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @GetMapping("/all")
    public List<Product> getProducts(){
        return productRepository.findAll();
    }

    @GetMapping("{id}")
    public Product getProduct(@PathVariable long id ){  return productRepository.getById(id); }

    @PostMapping
    public void addProduct(@RequestBody Product product) {
        Optional<Product> existingProduct =  productRepository.findById(product.getProductId());
        if(((Optional<?>) existingProduct).isEmpty())  productRepository.save(product);
    }

    @DeleteMapping("/{id}")
    public void removeProduct(@PathVariable long id ) {
        try {
            Product existingProduct  =  productRepository.findAll().stream().filter(p -> p.getProductId() == id).findFirst().get();
            productRepository.deleteById(id);
        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void updateProduct(@RequestBody Product product , @PathVariable long id ){
        try {
            Product existingProduct  =  productRepository.findAll().stream().filter(p -> p.getProductId() == id).findFirst().get();
            productRepository.save(product);
        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
        }
    }


    @GetMapping("/recommendations/{product_id}")
    public List<Recommendation> getProductRecommendations(@PathVariable long product_id)  {

        //TODO: Change URL
        WebClient client = WebClient.create("http://localhost:8080/api" );

        List<Recommendation> recommendations =
                client.get()
                        .uri("/recommendations/{product_id}", product_id )
                        .retrieve()
                        .bodyToFlux(Recommendation.class)
                        .collectList()
                        .block();

        return recommendations;

    }

    @PostMapping("/recommendation")
    public void addProductRecommendation(@RequestBody Recommendation recommendation) throws URISyntaxException {

        //TODO: Change URL

        WebClient client = WebClient.create("http://localhost:8080/api" );

        client
                .post()
                .uri("/recommendations")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(recommendation), Recommendation.class)
                .retrieve()
                .bodyToMono(Void.class)
                .block();

    }
}
