package com.example.v48;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class V48Application {

	public static void main(String[] args) {
		SpringApplication.run(V48Application.class, args);
	}

}
